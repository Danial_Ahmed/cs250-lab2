#include<iostream>			//Standar5d Input output Library
#include<vector>			//For Vectors
#include<algorithm>	//For Algorithm Sort
#include<ctime>
using namespace std;


bool updateTopScores(vector<int> &in, vector<int> & out){   //Our own Sort Function
	int temp = 0;			//Temp Variable for Swapping
	out = in;				//Copying input in Output to update Out put
	int n = out.size();		//Assigning n the value of vector size
	int j = 0;
	int prevI = 0;
	for (int i = 0; i<n - 1; i++)	//Loop for sorting
	{
		j++;		//Incrementing value of J
		if (j < n)	
		{
			i = prevI;
		}
		else
		{
			j = i + 1;
		}

		if (out[i] < out[j])		//If elements are not in descending order than swping them
		{
			int temp = out[i];
			out[i] = out[j];
			out[j] = temp;
		}
		prevI = i;

	}

	return true;
}


bool myfunction(int i, int j){ return i>j; }			//Function for sorting in Descending Order

bool TestCaseUpdated(){					//Test Case Function
	vector <int> Random(10000, 0);			//Random Vector
	vector <int> UpdatedRandom(10000, 0);	//Updated Random
	for (int i = 0; i < Random.size(); i++){		//Storing Random Numbers in Array
		Random[i] = 1 + rand() % 100;
	}
	sort(Random.begin(), Random.end(), myfunction);		//Sorting Through Sort
	cout << "\n\n\nSorted Through Algorithm & Printed\n";
	for (int i = 0; i < Random.size(); i++){

		cout << Random[i] << "   ";		//Printing
	}
	Random.push_back(rand() % 100);//Adding New value in the End 

	updateTopScores(Random, UpdatedRandom);
	int start_s = clock();

	updateTopScores(Random, UpdatedRandom);		//Sorting tghrough our Function

	int stop_s = clock();
	cout << "\n\ntime: " << (stop_s - start_s) / double(CLOCKS_PER_SEC) * 1000 << endl;
	sort(Random.begin(), Random.end(), myfunction);		//Sorting Through Sort

	cout << "\n\n\nCompared after adding New value in the end & Printing if Both are same\n\n";
	for (int i = 0; i < Random.size(); i++){
		if (Random[i] == UpdatedRandom[i])
			cout << Random[i] << "   ";
	} //Printing
	return true;
}
void main(){		//Main Function



	TestCaseUpdated();
}
